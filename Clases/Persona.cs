﻿using System.Globalization;

namespace Clases;

public class Persona
{

    public string nombre { get; set; } = "";
    public string documento { get; set; } = "";
    public string apellido { get; set; } = "";
    public DateTime fechaNacimiento { get; set; } = new DateTime();


    public string getFormatNacimiento()
    {
        return this.fechaNacimiento.ToString("dd/MM/yyyy");
    }

    public String getEdad()
    {
        int age = (int)Math.Floor((DateTime.Now - fechaNacimiento).TotalDays / 365.25D);

        return "" + age + "";

        //return this.fechaNacimiento.ToString("dd/MM/yyyy");
    }

    public void setNacimientoByString(String? fechaString)
    {
        try
        {
            CultureInfo culture = new CultureInfo("es-MX");
            this.fechaNacimiento = Convert.ToDateTime(fechaString, culture);
        } catch(Exception e)
        {
            Console.WriteLine("Error al parsear la fecha, intentalo nuevamente");
        }
    }

}

