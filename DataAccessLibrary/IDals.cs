﻿namespace DataAccessLibrary;
using Clases;

public interface IDals
{
    List<Persona> getPersonas();

    void Insertar(Persona persona);
}

