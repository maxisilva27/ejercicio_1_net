﻿using System;
using Clases;

namespace DataAccessLibrary
{
	public class Dals: IDals
	{
		public Dals()
		{
		}

        public List<Persona> personas = new List<Persona>();


        public void Insertar(Persona persona)
		{
			this.personas.Add(persona);
		}

        public List<Persona> getPersonas()
        {
            return this.personas;
        }
    }
}

