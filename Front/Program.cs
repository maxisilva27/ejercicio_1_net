﻿// See https://aka.ms/new-console-template for more information
using Clases;
using DataAccessLibrary;

Dals _capaDato = new Dals();

void mostrarMenuInsertar()
{
    do
    {
        try
        {
            Persona personaNueva = new Persona();
            Console.WriteLine("Ingrese nombre");
            personaNueva.nombre = Console.ReadLine();
            Console.WriteLine("Ingrese documento");
            personaNueva.documento = Console.ReadLine();
            Console.WriteLine("Ingrese un apellido");
            personaNueva.apellido = Console.ReadLine();

            Console.WriteLine("Ingrese una fecha de nacimiento DD/MM/YYYY");
            String fechaString = Console.ReadLine();
            personaNueva.setNacimientoByString(fechaString);
            _capaDato.Insertar(personaNueva);

            Console.WriteLine("Persona Insertada!");
            Console.WriteLine("----------------");
            Console.WriteLine("Nombre: " + personaNueva.nombre);
            Console.WriteLine("Apellido: " + personaNueva.apellido);
            Console.WriteLine("Documento: " + personaNueva.documento);
            Console.WriteLine("Nacimiento: " + personaNueva.getFormatNacimiento());

            Console.WriteLine("----------------");


        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            Console.WriteLine("Desea Continuear? S/N");
        }
    } while (Console.ReadLine().ToUpper() == "S");
    mostrarMenuPrincipal();
}

void mostrarPersonas()
{
    if (_capaDato.getPersonas().Count() == 0)
    {
       Console.WriteLine("No hay personas ingresadas");
    }
    _capaDato.getPersonas().ForEach((Persona p) =>
    {
        Console.WriteLine("Persona Insertada!");
        Console.WriteLine("----------------");
        Console.WriteLine("Nombre: " + p.nombre);
        Console.WriteLine("Apellido: " + p.apellido);
        Console.WriteLine("Documento: " + p.documento);
        Console.WriteLine("Edad: " + p.getEdad());
        Console.WriteLine("----------------");
    });
    Console.WriteLine("Presione una tecla para continuar");
    Console.ReadLine();
    mostrarMenuPrincipal();
}

void mostrarMenuPrincipal()
{
    Console.Clear();
    Console.WriteLine("Ingrese una de las siguientes opciones:");
    Console.WriteLine("1. Ver personas ingresadas");
    Console.WriteLine("2. Insertar personas");
    Console.WriteLine("Otra telca - Salir");

    String opcion = Console.ReadLine();

    if (opcion == "2") {
        mostrarMenuInsertar();
    } else if (opcion == "1") {
        mostrarPersonas();
    }

}

mostrarMenuPrincipal();

Console.WriteLine("Gracias por usar el sistema :) ");


